<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>Exercício 8</title>
</head>

<body>
  <div class="container" id="controle">
    <div class="list-group"><br />
      <div class="p-3 mb-2 bg-primary text-white" id="titulo" role="alert"><strong>
          EXERCÍCIO 8 - Arquivo TXT
        </strong>
      </div>
      <form method="POST" enctype="multipart/form-data" class="md-form">
        <div class="custom-file">
          <input type="file" name="arquivo" class="custom-file-input" id="customFile" required>
          <label class="custom-file-label" for="customFile">Selecione um arquivo</label>
        </div><br>
        <br><button type="submit" name="calcular" class="btn btn-primary">Arquivo
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-upload" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8zM5 4.854a.5.5 0 0 0 .707 0L8 2.56l2.293 2.293A.5.5 0 1 0 11 4.146L8.354 1.5a.5.5 0 0 0-.708 0L5 4.146a.5.5 0 0 0 0 .708z" />
            <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 2z" />
          </svg>
        </button>
        <a href="../estudo_php/index.php" class="btn btn-danger">Voltar todos exercícios
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-box-arrow-in-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M7.854 11.354a.5.5 0 0 0 0-.708L5.207 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z" />
            <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0 0 1h9A.5.5 0 0 0 15 8z" />
            <path fill-rule="evenodd" d="M2.5 14.5A1.5 1.5 0 0 1 1 13V3a1.5 1.5 0 0 1 1.5-1.5h8A1.5 1.5 0 0 1 12 3v1.5a.5.5 0 0 1-1 0V3a.5.5 0 0 0-.5-.5h-8A.5.5 0 0 0 2 3v10a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-1.5a.5.5 0 0 1 1 0V13a1.5 1.5 0 0 1-1.5 1.5h-8z" />
          </svg></a>
      </form>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
<script>
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>

</html>
<?php
if (isset($_FILES['arquivo']['tmp_name'])) {
  $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
  $teste = file($arquivo_tmp);
  $arquivo = implode($teste);
  $arr = str_split($arquivo);

  $arr = \array_diff(
    $arr,
    [" "],
    ["."],
    [","],
    ["-"],
    ["/"],
    ["?"],
    ["!"],
    ["#"],
    ["@"],
    ["$"],
    ["%"],
    ["&"],
    ["*"],
    ["+"],
    ["-"],
    ["<"],
    [">"],
    [";"],
    [":"],
    ["|"],
    ["¨"],
    ["{"],
    ["}"],
    ["["],
    ["]"],
    ["("],
    [")"],
    ["^"],
    ["~"],
    ["´"],
    ["`"],
    ["'"],
    ["_"],
    ["="],
    ["º"],
    ["ª"]
  );
  function array_icount_values($arr, $lower = true)
  {
    $arr2 = array();
    if (!is_array($arr['0'])) {
      $arr = array($arr);
    }
    foreach ($arr as $k => $v) {
      foreach ($v as $v2) {
        if ($lower == true) {
          $v2 = strtolower($v2);
        }
        if (!isset($arr2[$v2])) {
          $arr2[$v2] = 1;
        } else {
          $arr2[$v2]++;
        }
      }
    }
    return $arr2;
  }
  $res = array_icount_values($arr);
  foreach ($res as $key => $value) {
    $i = ucfirst($key);
    echo "<div class='container'><div class='alert alert-primary'><strong>Total de vezes que a letra '" . $i . "' apareceu no arquivo: " . $value . "</strong></div></div>";
  }
}
?>