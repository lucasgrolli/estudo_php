<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>Exercício 9</title>
</head>

<body>
  <div class="container" id="controle">
    <div class="list-group"><br />
      <div class="p-3 mb-2 bg-primary text-white" id="titulo" role="alert"><strong>
          EXERCÍCIO 9 - Combinações de palavras
        </strong>
      </div>
      <form method="POST">
        <div class="form-group">
          <label for="exampleInputVal1">Palavra </label>
          <input type="text" class="form-control" name="nome" placeholder="Digite uma palavra" required>
        </div>
        <button type="submit" name="combinar" class="btn btn-primary">Combinar Palavras
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-bar-contract" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M3.646 14.854a.5.5 0 0 0 .708 0L8 11.207l3.646 3.647a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 0 0 0 .708zm0-13.708a.5.5 0 0 1 .708 0L8 4.793l3.646-3.647a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 0-.708zM1 8a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 8z" />
          </svg>
        </button>
        <a href="../estudo_php/index.php" class="btn btn-danger">Voltar todos exercícios
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-box-arrow-in-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M7.854 11.354a.5.5 0 0 0 0-.708L5.207 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z" />
            <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0 0 1h9A.5.5 0 0 0 15 8z" />
            <path fill-rule="evenodd" d="M2.5 14.5A1.5 1.5 0 0 1 1 13V3a1.5 1.5 0 0 1 1.5-1.5h8A1.5 1.5 0 0 1 12 3v1.5a.5.5 0 0 1-1 0V3a.5.5 0 0 0-.5-.5h-8A.5.5 0 0 0 2 3v10a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-1.5a.5.5 0 0 1 1 0V13a1.5 1.5 0 0 1-1.5 1.5h-8z" />
          </svg></a>
      </form>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>
<?php
if (isset($_POST['combinar'])) {
  $input = $_POST['nome'];
  function string_getpermutations($prefix, $characters, &$permutations)
  {
    if (count($characters) == 1)
      $permutations[] = $prefix . array_pop($characters);
    else {
      for ($i = 0; $i < count($characters); $i++) {
        $tmp = $characters;
        unset($tmp[$i]);
        string_getpermutations($prefix . $characters[$i], array_values($tmp), $permutations);
      }
    }
  }
  $characters = array();
  for ($i = 0; $i < strlen($input); $i++)
    $characters[] = $input[$i];
  $permutations = array();
  string_getpermutations("", $characters, $permutations);
  echo "<br><div class='container'><div class='alert alert-info'><strong>Essas são as seguintes combinações  </strong><br>"
    . implode(", ", $permutations) . "<br></div></div>";
}
?>