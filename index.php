<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>Exercícios básicos PHP</title>
</head>

<body>

  <div class="container">
    <div class="list-group" id="controle"><br />
      <strong><a class="list-group-item list-group-item-action active">
          Exercícios PHP - Lucas Grolli de Oliveira
        </a></strong>
      <a href="../estudo_php/ex1.php" class="list-group-item list-group-item-action">Exercício 1 - Tipos de ordenação</a>
      <a href="../estudo_php/ex2.php" class="list-group-item list-group-item-action">Exercício 2 - Média Aritmética</a>
      <a href="../estudo_php/ex3.php" class="list-group-item list-group-item-action">Exercício 3 - Preço de Combustível</a>
      <a href="../estudo_php/ex4.php" class="list-group-item list-group-item-action">Exercício 4 - Cálculo de IMC</a>
      <a href="../estudo_php/ex5.php" class="list-group-item list-group-item-action">Exercício 5 - Valores de 0 até o número digitado</a>
      <a href="../estudo_php/ex6.php" class="list-group-item list-group-item-action">Exercício 6 - Fatorial</a>
      <a href="../estudo_php/ex7.php" class="list-group-item list-group-item-action">Exercício 7 - Valor Intermediário</a>
      <a href="../estudo_php/ex8.php" class="list-group-item list-group-item-action">Exercício 8 - Leitura arquivo txt</a>
      <a href="../estudo_php/ex9.php" class="list-group-item list-group-item-action">Exercício 9 - Combinações de palavras</a>
      <a href="../estudo_php/ex10.php" class="list-group-item list-group-item-action">Exercício 10 - Reverter ordem dos números</a>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>