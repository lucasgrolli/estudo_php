<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>Exercício 1</title>
</head>

<body>
  <div class="container" id="controle">
    <div class="list-group"><br />
      <div class="p-3 mb-2 bg-primary text-white" id="titulo" role="alert"><strong>
          EXERCÍCIO 1 - Tipos de ordenação 
        </strong>
      </div>
      <form method="POST">
        <label for="exampleFormControlSelect1">Tipo ordenação</label>
        <select class="form-control" name="order" id="exampleFormControlSelect1">
          <option value="0"></option>
          <option value="1">Blubble Sort</option>
          <option value="2">Quick Sort</option>
          <option value="3">Merge Sort</option>
        </select>
        <br>
        <div class="form-group">
          <label for="exampleInputVal1">Valor 1 </label>
          <input type="text" class="form-control" name="valores[]" required>
        </div>
        <div class="form-group">
          <label for="exampleInputVal1">Valor 2 </label>
          <input type="text" class="form-control" name="valores[]" required>
        </div>
        <div class="form-group">
          <label for="exampleInputVal1">Valor 3 </label>
          <input type="text" class="form-control" name="valores[]" required>
        </div>
        <div class="form-group">
          <label for="exampleInputVal1">Valor 4 </label>
          <input type="text" class="form-control" name="valores[]" required>
        </div>
        <div class="form-group">
          <label for="exampleInputVal1">Valor 5 </label>
          <input type="text" class="form-control" name="valores[]" required>
        </div>
        <button type="submit" class="btn btn-primary">Ordenar
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check2-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M15.354 2.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L8 9.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
            <path fill-rule="evenodd" d="M1.5 13A1.5 1.5 0 0 0 3 14.5h10a1.5 1.5 0 0 0 1.5-1.5V8a.5.5 0 0 0-1 0v5a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V3a.5.5 0 0 1 .5-.5h8a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 3v10z" />
          </svg>
        </button>
        <a href="../estudo_php/index.php" class="btn btn-danger">Voltar todos exercícios
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-box-arrow-in-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M7.854 11.354a.5.5 0 0 0 0-.708L5.207 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z" />
            <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0 0 1h9A.5.5 0 0 0 15 8z" />
            <path fill-rule="evenodd" d="M2.5 14.5A1.5 1.5 0 0 1 1 13V3a1.5 1.5 0 0 1 1.5-1.5h8A1.5 1.5 0 0 1 12 3v1.5a.5.5 0 0 1-1 0V3a.5.5 0 0 0-.5-.5h-8A.5.5 0 0 0 2 3v10a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-1.5a.5.5 0 0 1 1 0V13a1.5 1.5 0 0 1-1.5 1.5h-8z" />
          </svg></a>
      </form>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>
<?php
if (isset($_POST) && !empty($_POST)) {
  $v = $_POST["valores"];
  $o = $_POST["order"];

  switch ($o) {
    case 0:
      echo "<div class='container'><div class='alert alert-danger'><strong>TIPO DE ORDENAÇÃO SELECIONADA NÃO É VÁLIDA</strong></div></div>";
      break;
    case 1:
      function bubbleSort($v)
      {
        $tamanho = count($v);
        $limite = $tamanho - 1;
        $compara  = 0;
        for ($i = 0; $i < $tamanho; $i++) {
          $flag  = false;
          $novoLimite  = 0;
          for ($j = 0; $j < $limite; $j++) {
            $compara++;
            if ($v[$j] > $v[$j + 1]) {
              $tmp = $v[$j + 1];
              $v[$j + 1] = $v[$j];
              $v[$j] = $tmp;
              $flag = true;
              $novoLimite = $j;
            }
          }
          $limite = $novoLimite;
          if (!$flag) {
            break;
          }
        }
        echo '<h4>Total de comparações: ' . $compara . '</h4>';
        return $v;
      }
      echo "<div class='container'><div class='alert alert-info'><strong>Valores digitados antes da ordenação</strong><br>" . implode(', ', $v) . '<br>';
      $sorted = bubbleSort($v);
      echo '<strong>Depois da ordenação por Bubble Sort</strong><br>' . implode(', ', $sorted);
      break;

    case 2:
      function partition(&$v, $leftIndex, $rightIndex){
        $pivot = $v[($leftIndex + $rightIndex) / 2];

        while ($leftIndex <= $rightIndex) {
          while ($v[$leftIndex] < $pivot)
            $leftIndex++;
          while ($v[$rightIndex] > $pivot)
            $rightIndex--;
          if ($leftIndex <= $rightIndex) {
            $tmp = $v[$leftIndex];
            $v[$leftIndex] = $v[$rightIndex];
            $v[$rightIndex] = $tmp;
            $leftIndex++;
            $rightIndex--;
          }
        }
        echo implode(", " , $v) . " - <strong>Pivo $pivot</strong><br>";
        return $leftIndex;
      }
      function quickSort(&$v, $leftIndex, $rightIndex) {
        $index = partition($v, $leftIndex, $rightIndex);
        if ($leftIndex < $index - 1)
          quickSort($v, $leftIndex, $index - 1);
        if ($index < $rightIndex)
          quickSort($v, $index, $rightIndex);
      }
      echo "<div class='container'><div class='alert alert-info'><strong>Valores digitados antes da ordenação</strong><br>" . implode(', ', $v) . '<br>';
      quickSort($v, 0, count($v) - 1);
      echo '<strong>Depois da ordenação por Quick Sort</strong><br>' . implode(', ', $v);
      break;

    case 3:
      function merge_sort($v){
        if(count($v) == 1 ) return $v;
        $mid = count($v) / 2;
          $left = array_slice($v, 0, $mid);
          $right = array_slice($v, $mid);
        $left = merge_sort($left);
        $right = merge_sort($right);
        return merge($left, $right);
      }
      function merge($left, $right){
        $res = array();
        while (count($left) > 0 && count($right) > 0){
          if($left[0] > $right[0]){
            $res[] = $right[0];
            $right = array_slice($right , 1);
          }else{
            $res[] = $left[0];
            $left = array_slice($left, 1);
          }
        }
        while (count($left) > 0){
          $res[] = $left[0];
          $left = array_slice($left, 1);
        }
        while (count($right) > 0){
          $res[] = $right[0];
          $right = array_slice($right, 1);
        }
        return $res;
      }
      $result = merge_sort($v);
      echo "<div class='container'><div class='alert alert-info'><strong>Valores digitados antes da ordenação</strong><br>". implode(', ', $v);
      echo '<br><strong>Depois da ordenação por Merge Sort</strong><br>' . implode(', ', $result) . "</div></div>";    
    break;
  }
}
?>